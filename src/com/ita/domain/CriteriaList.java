package com.ita.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.ita.persistence.SandwichDAO;

public class CriteriaList implements Serializable, SandwichDAO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static Integer count = 0;	
	
	private Integer id;
	private String name;
	private List<Sandwich> criteriaList;
		
	public CriteriaList(){
		count++;		
		this.id = count;
	}

	public CriteriaList(String name) {
		super();
		count++;
				
		this.id = count;
		this.name = name;
		this.criteriaList = new ArrayList<Sandwich>();
	}

	public Integer getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public List<Sandwich> getCriteriaList() {
		return criteriaList;
	}

	public void setCriteriaList(List<Sandwich> criteriaList) {
		this.criteriaList = criteriaList;
	}

	@Override
	public String toString() {
		System.out.println(String.format("id <%d>, name is - %s", id, getName()));
		showList(getCriteriaList(), "List of sandwiches");
		String space = "";
		return space;
				
	}
	
	@Override
	public boolean add(Sandwich sandwich){
		if (!criteriaList.contains(sandwich)) {			
			criteriaList.add(sandwich);
			return true;
		}
		return false;
	}
	
	@Override
	public boolean remove(Sandwich sandwich) {
		if (criteriaList.contains(sandwich)) {
			criteriaList.remove(sandwich);
		}		
		return criteriaList.contains(sandwich);
	}

	@Override
	public boolean update(Sandwich sandwich, String name, Double price,
			String size) {
		Integer id = sandwich.getId();
		for (Sandwich sandwichInList : criteriaList) {
			if (sandwichInList.getId().equals(id)) {
				sandwichInList.setName(name);
				sandwichInList.setPrice(price);
				sandwichInList.setSize(size);					
			}		
		}
		return criteriaList.contains(sandwich);		
	}

	@Override
	public Sandwich getById(Integer id) {
		for (Sandwich sandwich : criteriaList) {
			if (sandwich.getId().equals(id)) {
				return sandwich;
			}
		} return null;
	}

	@Override
	public List<Sandwich> getAll() {
		return criteriaList;
	}

	@Override
	public List<Sandwich> getByPrice(Double price) {
		List<Sandwich> listByPrice = new ArrayList<Sandwich>();
		for (Sandwich sandwich : criteriaList) {
			if (sandwich.getPrice().equals(price)) {
				listByPrice.add(sandwich);
			}			
		}
		return listByPrice;
	}

	public static void showList(List<Sandwich> list, String message) {
		System.out.println(message);
		System.out.println(" ");
		
		for (Sandwich sandwich : list) {
			System.out.println(sandwich);
		}
		System.out.println(" ");
	}	
	//�������� �������� � ���� �������
	

}
