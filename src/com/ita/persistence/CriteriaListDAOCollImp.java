package com.ita.persistence;

import java.util.ArrayList;
import java.util.List;

import com.ita.domain.CriteriaList;
import com.ita.domain.Sandwich;

public class CriteriaListDAOCollImp implements CriteriaListDAO {
	
	private static List<CriteriaList> criteriaDepot = new ArrayList<>();

	@Override
	public boolean add(CriteriaList criteria) {
		if (!criteriaDepot.contains(criteria)) {			
			criteriaDepot.add(criteria);
			return true;
		}
		return false;
	}

	@Override
	public boolean remove(CriteriaList criteria) {
		if (criteriaDepot.contains(criteria)){
				criteriaDepot.remove(criteria);			
		}
		return criteriaDepot.contains(criteria);
	}

	@Override
	public boolean update(CriteriaList criteria, String name) {
		Integer id = criteria.getId();
		for (CriteriaList criteriaInList : criteriaDepot) {
			if (criteriaInList.getId().equals(id)) {
				criteriaInList.setName(name);									
			}		
		}
		return criteriaDepot.contains(criteria);
	}


	@Override
	public CriteriaList getById(Integer id) {
		for (CriteriaList criteria : criteriaDepot) {
			if (criteria.getId().equals(id)) {
				return criteria;
			}
		} return null;		
	}

	@Override
	public List<CriteriaList> getAll() {		
		return criteriaDepot;
	}
	
	

}
