package com.ita.persistence;

import java.util.List;

import com.ita.domain.Sandwich;
import com.sun.jndi.url.corbaname.corbanameURLContextFactory;

import java.util.ArrayList;
import java.io.*;

public class SandwichDAOSerImp implements SandwichDAO{	
	
	public static void doSerialization(ArrayList<Sandwich> menu){
		serialize(menu, "sandwich1");
	}
	
	private static ArrayList<Sandwich> serialize(ArrayList<Sandwich> menu, String fileName){
    
		try{
			FileOutputStream fos = new FileOutputStream(fileName);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			oos.writeObject(menu);
			oos.close();
			fos.close();
			
			return menu;
		}catch(IOException ioe){
			ioe.printStackTrace();
			return null;
		}
	}
	
	private static ArrayList<Sandwich> deSerialize(String fileName){
		
		ArrayList<Sandwich> menu = new ArrayList<>();
		
		try{
			FileInputStream fis = new FileInputStream(fileName);
			ObjectInputStream ois = new ObjectInputStream(fis);
			menu = (ArrayList<Sandwich>) ois.readObject();
			
			ois.close();
			fis.close();
			
			return menu;
		}catch(IOException ioe){
			ioe.printStackTrace();
			return null;
		}catch(ClassNotFoundException c){
			System.out.println("Class not found");
			c.printStackTrace();
			return null;
		}
	}
	
	@Override
	public boolean add(Sandwich sandwich) {
		ArrayList<Sandwich> menu = deSerialize("sandwich1");
		
		
		if (!menu.contains(sandwich)) {			
			menu.add(sandwich);
			serialize(menu, "sandwich1");
			return true;
		}
		return false;		
	}

	@Override
	public boolean remove(Sandwich sandwich) {
		ArrayList<Sandwich> menu = deSerialize("sandwich1");

		if (menu.contains(sandwich)) {
			menu.remove(sandwich);
		}
		serialize(menu, "sandwich1");
		return menu.contains(sandwich);
	}

	@Override
	public boolean update(Sandwich sandwich, String name, Double price,
			String size) {
		ArrayList<Sandwich> menu = deSerialize("sandwich1");
		
		Integer id = sandwich.getId();
		for (Sandwich sandwichInList : menu) {
			if (sandwichInList.getId().equals(id)) {
				sandwichInList.setName(name);
				sandwichInList.setPrice(price);
				sandwichInList.setSize(size);					
			}		
		}
		serialize(menu,"sandwich1");
		return menu.contains(sandwich);
	}

	@Override
	public Sandwich getById(Integer id) {
		ArrayList<Sandwich> menu = deSerialize("sandwich1");
		
		for (Sandwich sandwich : menu) {
			if (sandwich.getId().equals(id)) {
				return sandwich;
			}
		} return null;		
	}
		

	@Override
	public List<Sandwich> getAll() {
		ArrayList<Sandwich> menu = deSerialize("sandwich1");
		return menu;
	}

	@Override
	public List<Sandwich> getByPrice(Double price) {
		ArrayList<Sandwich> menu = deSerialize("sandwich1");
		
		List<Sandwich> listByPrice = new ArrayList<Sandwich>();
		for (Sandwich sandwich : menu) {
			if (sandwich.getPrice().equals(price)) {
				listByPrice.add(sandwich);
			}			
		}
				
		return listByPrice;
	}
		
		
}
