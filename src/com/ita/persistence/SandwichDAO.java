package com.ita.persistence;

import java.util.List;

import com.ita.domain.Sandwich;

public interface SandwichDAO {

	//1) as a manager i want to add new sandwich to the menu;
	boolean add(Sandwich sandwich);	
	//2) as a manager I want to remove a sandwich from menu;
	boolean remove(Sandwich sandwich);
	//3) as a manager I want to change a recipe of certain sandwich;
	boolean update(Sandwich sandwich, String name, Double price, String size);
	//4) as a costumer I want to order a sandwich;
	Sandwich getById(Integer id);
	//6) as a coustumer/manager I want to see all avalible sandwiches;
	List<Sandwich> getAll();
	//7) as a coustumer/manager I want to see all avalible sandwiches with paricular price;
	List<Sandwich> getByPrice(Double price);
}
