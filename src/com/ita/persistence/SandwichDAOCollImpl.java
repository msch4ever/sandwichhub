package com.ita.persistence;

import java.util.ArrayList;
import java.util.List;

import com.ita.domain.Sandwich;

public class SandwichDAOCollImpl implements SandwichDAO{
	
	private static List<Sandwich> menu = new ArrayList<>();
	//private Integer id = 0;
	

	@Override
	public boolean add(Sandwich sandwich) {
		if (!menu.contains(sandwich)) {			
			menu.add(sandwich);
			return true;
		}
		return false;
	}

	@Override
	public boolean remove(Sandwich sandwich) {
		if (menu.contains(sandwich)) {
			menu.remove(sandwich);
		}		
		return menu.contains(sandwich);
	}

	@Override
	public boolean update(Sandwich sandwich, String name, Double price, String size) {
		Integer id = sandwich.getId();
		for (Sandwich sandwichInList : menu) {
			if (sandwichInList.getId().equals(id)) {
				sandwichInList.setName(name);
				sandwichInList.setPrice(price);
				sandwichInList.setSize(size);					
			}		
		}
		return menu.contains(sandwich);
	}

	@Override
	public Sandwich getById(Integer id) {		
		for (Sandwich sandwich : menu) {
			if (sandwich.getId().equals(id)) {
				return sandwich;
			}
		} return null;		
	}

	@Override
	public List<Sandwich> getAll() {		
		return menu;
	}

	@Override
	public List<Sandwich> getByPrice(Double price) {
		List<Sandwich> listByPrice = new ArrayList<Sandwich>();
		for (Sandwich sandwich : menu) {
			if (sandwich.getPrice().equals(price)) {
				listByPrice.add(sandwich);
			}			
		}
		return listByPrice;
	}
	

}
