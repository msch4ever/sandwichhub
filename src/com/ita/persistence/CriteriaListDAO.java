package com.ita.persistence;

import java.util.List;

import com.ita.domain.CriteriaList;;

public interface CriteriaListDAO {
	    //1) as a manager i want to add new Criteria to the menu;
		boolean add(CriteriaList criteria);	
		//2) as a manager I want to remove a Criteria  from menu;
		boolean remove(CriteriaList criteria);
		//3) as a manager I want to update certain Criteria;
		boolean update(CriteriaList criteria, String name);
		//4) as a costumer I want to get a Criteria;
		CriteriaList getById(Integer id);
		//6) as a coustumer/manager I want to see all avalible Criteria;
		List<CriteriaList> getAll();		
	}
