package com.ita.persistence;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import com.ita.domain.CriteriaList;
import com.ita.domain.Sandwich;

public class CriteriaListDAOSerImpl implements CriteriaListDAO {

	public static void doSerialization(ArrayList<CriteriaList> criteriaDepot){
		serialize(criteriaDepot, "criteriaDepot1");
	}
	
	private static ArrayList<CriteriaList> serialize(ArrayList<CriteriaList> criteriaList, String fileName){
	    
		try{
			FileOutputStream fos = new FileOutputStream(fileName);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			
			oos.writeObject(criteriaList);
			oos.close();
			fos.close();
			
			return criteriaList;
		}catch(IOException ioe){
			ioe.printStackTrace();
			return null;
		}
	}
	
	private static ArrayList<CriteriaList> deSerialize(String fileName){
		
		ArrayList<CriteriaList> criteriaDepot = new ArrayList<>();
		
		try{
			FileInputStream fis = new FileInputStream(fileName);
			ObjectInputStream ois = new ObjectInputStream(fis);
			criteriaDepot = (ArrayList<CriteriaList>) ois.readObject();
			
			ois.close();
			fis.close();
			
			return criteriaDepot;
		}catch(IOException ioe){
			ioe.printStackTrace();
			return null;
		}catch(ClassNotFoundException c){
			System.out.println("Class not found");
			c.printStackTrace();
			return null;
		}
	}
	
	@Override
	public boolean add(CriteriaList criteria) {
		ArrayList<CriteriaList> criteriaDepot = deSerialize("criteriaDepot1");
				
		if (!criteriaDepot.contains(criteria)) {			
			criteriaDepot.add(criteria);
			serialize(criteriaDepot, "criteriaDepot1");
			return true;
		}else{
			System.out.println("this criteria is already in the list");
		}
		return false;
	}

	@Override
	public boolean remove(CriteriaList criteria) {
		ArrayList<CriteriaList> criteriaDepot = deSerialize("criteriaDepot1");
				
		if (criteriaDepot.contains(criteria)){
			criteriaDepot.remove(criteria);	
			serialize(criteriaDepot, "criteriaDepot1");
	}
	return criteriaDepot.contains(criteria);
}

	@Override
	public boolean update(CriteriaList criteria, String name) {
		ArrayList<CriteriaList> criteriaDepot = deSerialize("criteriaDepot1");
		
		Integer id = criteria.getId();
		for (CriteriaList criteriaInList : criteriaDepot) {
			if (criteriaInList.getId().equals(id)) {
				criteriaInList.setName(name);
				
				serialize(criteriaDepot, "criteriaDepot1");
			}		
		}
		return criteriaDepot.contains(criteria);
	}

	@Override
	public CriteriaList getById(Integer id) {
		ArrayList<CriteriaList> criteriaDepot = deSerialize("criteriaDepot1");
		
		for (CriteriaList criteria : criteriaDepot) {
			if (criteria.getId().equals(id)) {
				return criteria;
			}
		} return null;		
	}

	@Override
	public List<CriteriaList> getAll() {
		ArrayList<CriteriaList> criteriaDepot = deSerialize("criteriaDepot1");
		return criteriaDepot;
	}
	
	

}
