package com.ita;

import java.util.ArrayList;
import java.util.List;

import com.ita.domain.Sandwich;
import com.ita.domain.CriteriaList;
import com.ita.persistence.SandwichDAOCollImpl;

public class App {

	public static void main(String[] args) {
		
		SandwichDAOCollImpl repo = new SandwichDAOCollImpl();
		
		Sandwich s1 = new Sandwich("BMT", 5.00, "footlong");		
		Sandwich s2 = new Sandwich("BLT", 5.00, "footlong");
		Sandwich s3 = new Sandwich("Turkey", 5.00, "footlong");
		Sandwich s4 = new Sandwich("Meatball", 6.50, "footlong");
		Sandwich s5 = new Sandwich("Ham", 4.50, "6inch");
		Sandwich s6 = new Sandwich("Chicken&Bacon&Ranch", 5.50, "6inch");
		Sandwich s7 = new Sandwich("BBQ Pulled Pork", 5.50, "6inch");
		Sandwich s8 = new Sandwich("Fist Of Meat", 9.00, "footlong");
		Sandwich s9 = new Sandwich("Chicken brest", 5.00, "6inch");
		Sandwich s10 = new Sandwich("Chicken terreyaky", 7.00, "footlong");
		
	

		

		repo.add(s1);
		repo.add(s2);
		repo.add(s3);
		repo.add(s4);
		repo.add(s5);
		repo.add(s6);
		repo.add(s7);
		repo.add(s8);
		repo.add(s9);
		repo.add(s10);
		
		
		//getAll test
		List<Sandwich> list =(ArrayList<Sandwich>) repo.getAll();
		
		showList(list, "List of all sandwiches: ");
		
		//getByPrice test
		ArrayList<Sandwich> list2 =(ArrayList<Sandwich>) repo.getByPrice(7.0D);
		showList(list2, "List of all sandwiches by price" );
		
		//remove test
		repo.remove(s2);
		showList(list, "List after remove: ");
		
		//update test
		repo.update(s3, "newTurkey", 5.5D, "6 inch");
		showList(list, "List after update: ");
		
		//getById test
		System.out.println(repo.getById(6));
		
		//test SortList		
		CriteriaList chicken = new CriteriaList("chicken");
		CriteriaList sixInch = new CriteriaList("sixInch");

		chicken.add(s6);
		chicken.add(s10);
		chicken.add(s9);
		
		sixInch.add(s5);
		sixInch.add(s6);
		sixInch.add(s7);
		sixInch.add(s9);
		
		//test SortList add()
		showList((ArrayList<Sandwich>)chicken.getCriteriaList(), "List of chichken subs: ");
		showList((ArrayList<Sandwich>)sixInch.getCriteriaList(), "List of sixInch subs: ");

		list.add(1, s2);
		showList(list, "List of all sandwiches: ");
		
		//test CriteriaLIst getByPrice
		ArrayList<Sandwich> list3 =(ArrayList<Sandwich>) chicken.getByPrice(5.5D);
		showList(list3, "List of all sandwiches by price" );
		
		ArrayList<Sandwich> list4 =(ArrayList<Sandwich>) sixInch.getByPrice(5.5D);
		showList(list4, "List of all sandwiches by price" );
				
		
	}

	public static void showList(List<Sandwich> list, String message) {
		System.out.println(message);
		System.out.println(" ");
		
		for (Sandwich sandwich : list) {
			System.out.println(sandwich);
		}
		System.out.println(" ");
	}

}
