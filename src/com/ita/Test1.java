package com.ita;

import java.util.List;

import com.ita.domain.CriteriaList;
import com.ita.domain.Sandwich;
import com.ita.persistence.SandwichDAOCollImpl;
import com.ita.persistence.CriteriaListDAOCollImp;

public class Test1 {

	public static void main(String[] args) {
		SandwichDAOCollImpl sandwichDepot = new SandwichDAOCollImpl();
		CriteriaListDAOCollImp criteriaDepot = new CriteriaListDAOCollImp();
		
		Sandwich s1 = new Sandwich("BMT", 5.00, "footlong");
		Sandwich s2 = new Sandwich("BMT", 4.50, "6 inch");
		
		Sandwich s3 = new Sandwich("Salami", 5.00, "footlong");
		Sandwich s4 = new Sandwich("Salami", 4.50, "6 inch");
		
		Sandwich s5 = new Sandwich("Peperoni", 5.00, "footlong");
		Sandwich s6 = new Sandwich("Peperoni", 4.50, "6 inch");
		
		Sandwich s7 = new Sandwich("BLT", 5.00, "footlong");
		Sandwich s8 = new Sandwich("BLT", 4.50, "6 inch");
		
		Sandwich s9 = new Sandwich("Turkey", 7.00, "footlong");
		Sandwich s10 = new Sandwich("Turkey", 5.50, "6 inch");
		
		Sandwich s11 = new Sandwich("Ham", 6.00, "footlong");
		Sandwich s12 = new Sandwich("Ham", 5.00, "6 inch");
		
		Sandwich s13 = new Sandwich("Tuna", 6.00, "footlong");
		Sandwich s14 = new Sandwich("Tuna", 5.00, "6 inch");
		
		Sandwich s15 = new Sandwich("Chicken&Bacon&Ranch", 7.00, "footlong");
		Sandwich s16 = new Sandwich("Chicken&Bacon&Ranch", 5.50, "6 inch");
		
		Sandwich s17 = new Sandwich("Chicken brest", 5.50, "footlong");
		Sandwich s18 = new Sandwich("Chicken brest", 4.50, "6 inch");
		
		Sandwich s19 = new Sandwich("Chicken terreyaky", 7.50, "footlong");
		Sandwich s20 = new Sandwich("Chicken terreyaky", 6.00, "6 inch");
		
		Sandwich s21 = new Sandwich("BBQ Pulled Pork", 7.50, "footlong");
		Sandwich s22 = new Sandwich("BBQ Pulled Pork", 6.00, "6 inch");
		
		Sandwich s23 = new Sandwich("Fist Of Meat", 9.00, "footlong");
		Sandwich s24 = new Sandwich("Fist Of Meat", 7.50, "6 inch");
		
		Sandwich s25 = new Sandwich("Meatball marrinara", 5.00, "footlong");
		Sandwich s26 = new Sandwich("Meatball marrinara", 4.50, "6 inch");
		
		sandwichDepot.add(s1);
		sandwichDepot.add(s2);
		sandwichDepot.add(s3);
		sandwichDepot.add(s4);
		sandwichDepot.add(s5);
		sandwichDepot.add(s6);
		sandwichDepot.add(s7);
		sandwichDepot.add(s8);
		sandwichDepot.add(s9);
		sandwichDepot.add(s10);		
		sandwichDepot.add(s11);
		sandwichDepot.add(s12);
		sandwichDepot.add(s13);
		sandwichDepot.add(s14);
		sandwichDepot.add(s15);
		sandwichDepot.add(s16);
		sandwichDepot.add(s17);
		sandwichDepot.add(s18);		
		sandwichDepot.add(s19);
		sandwichDepot.add(s20);
		sandwichDepot.add(s21);
		sandwichDepot.add(s22);
		sandwichDepot.add(s23);
		sandwichDepot.add(s24);
		sandwichDepot.add(s25);
		sandwichDepot.add(s26);
		
		System.out.println("there are "+sandwichDepot.getAll().size()+" sandwiches in menu");

		CriteriaList chicken = new CriteriaList("chicken");
		CriteriaList sausage = new CriteriaList("sausage");
		CriteriaList meaty = new CriteriaList("meaty");
		//CriteriaList forKids = new CriteriaList("forKids");
		CriteriaList sixInch = new CriteriaList("6 Inch");
		CriteriaList footlong = new CriteriaList("footlong");
		//CriteriaList fiveBucks = new CriteriaList("fiveBucks");
		//CriteriaList fiveBucksFootlong = new CriteriaList("fiveBucksFootlong");
		
		chicken.add(s15);
		chicken.add(s16);
		chicken.add(s17);
		chicken.add(s18);
		chicken.add(s19);
		chicken.add(s20);
		
		sausage.add(s1);
		sausage.add(s2);
		sausage.add(s3);
		sausage.add(s4);
		sausage.add(s5);
		sausage.add(s6);
		sausage.add(s23);
		sausage.add(s24);
		
		meaty.add(s1);
		meaty.add(s2);
		meaty.add(s7);
		meaty.add(s8);
		meaty.add(s21);
		meaty.add(s22);
		meaty.add(s23);
		meaty.add(s24);
		
		sixInch.add(s2);
		sixInch.add(s4);
		sixInch.add(s6);
		sixInch.add(s8);
		sixInch.add(s10);
		sixInch.add(s12);
		sixInch.add(s14);
		sixInch.add(s16);
		sixInch.add(s18);
		sixInch.add(s20);
		sixInch.add(s22);
		sixInch.add(s24);
		sixInch.add(s26);
		
		footlong.add(s1);
		footlong.add(s3);
		footlong.add(s5);
		footlong.add(s7);
		footlong.add(s9);
		footlong.add(s11);
		footlong.add(s13);
		footlong.add(s15);
		footlong.add(s17);
		footlong.add(s19);
		footlong.add(s21);
		footlong.add(s23);
		footlong.add(s25);
		
		//shows that lists aren't empty
		
		showListSandwich(chicken.getAll(), "Here are chicken subs");
		System.out.println("there are "+sausage.getAll().size()+" sausage sandwiches in menu");
		System.out.println("there are "+sixInch.getAll().size()+" 6 inch sandwiches in menu");
		System.out.println("there are "+footlong.getAll().size()+" footlong sandwiches in menu");
		
		//add to criteriaDepo
		
		criteriaDepot.add(chicken);
		criteriaDepot.add(sausage);
		criteriaDepot.add(sixInch);
		criteriaDepot.add(footlong);
		
		//add test
		showListCriteria(criteriaDepot.getAll(), "All criteria");
		//getById test
		System.out.println(criteriaDepot.getById(2).toString());
		//remove test
		System.out.println("REMOOOOOOVE test");
		criteriaDepot.remove(chicken);
		showListCriteria(criteriaDepot.getAll(), "All criteria");
		
			
		//showListCriteria(criteriaDepot.getAll(), "All criteria");

		System.out.println(criteriaDepot.getAll().size());		

	}
	
	public static void showListSandwich(List<Sandwich> list, String message) {
		System.out.println(message);
		System.out.println(" ");
		
		for (Sandwich sandwich : list) {
			System.out.println(sandwich);
		}
		System.out.println(" ");
	}
	public static void showListCriteria(List<CriteriaList> list, String message) {
		System.out.println(message);
		System.out.println(" ");
		
		for (CriteriaList criteria : list) {
			System.out.println(criteria);
		}
		System.out.println(" ");
	}


}
