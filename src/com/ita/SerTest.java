package com.ita;

import java.util.ArrayList;

import com.ita.domain.Sandwich;
import com.ita.persistence.SandwichDAOSerImp;

public class SerTest {
	
	public static void main(String[] args) {
	SandwichDAOSerImp sandwichDepot = new SandwichDAOSerImp();
	ArrayList<Sandwich> tamplate = new ArrayList<Sandwich>();
	
	SandwichDAOSerImp.doSerialization(tamplate);
	
	Sandwich s1 = new Sandwich("BMT", 5.00, "footlong");
	Sandwich s2 = new Sandwich("BMT", 4.50, "6 inch");
	
	Sandwich s3 = new Sandwich("Salami", 5.00, "footlong");
	Sandwich s4 = new Sandwich("Salami", 4.50, "6 inch");
	
	Sandwich s5 = new Sandwich("Peperoni", 5.00, "footlong");
	Sandwich s6 = new Sandwich("Peperoni", 4.50, "6 inch");
	
	Sandwich s7 = new Sandwich("BLT", 5.00, "footlong");
	Sandwich s8 = new Sandwich("BLT", 4.50, "6 inch");
	
	Sandwich s9 = new Sandwich("Turkey", 7.00, "footlong");
	Sandwich s10 = new Sandwich("Turkey", 5.50, "6 inch");
	
	Sandwich s11 = new Sandwich("Ham", 6.00, "footlong");
	Sandwich s12 = new Sandwich("Ham", 5.00, "6 inch");
	
	Sandwich s13 = new Sandwich("Tuna", 6.00, "footlong");
	Sandwich s14 = new Sandwich("Tuna", 5.00, "6 inch");
	
	Sandwich s15 = new Sandwich("Chicken&Bacon&Ranch", 7.00, "footlong");
	Sandwich s16 = new Sandwich("Chicken&Bacon&Ranch", 5.50, "6 inch");
	
	Sandwich s17 = new Sandwich("Chicken brest", 5.50, "footlong");
	Sandwich s18 = new Sandwich("Chicken brest", 4.50, "6 inch");
	
	Sandwich s19 = new Sandwich("Chicken terreyaky", 7.50, "footlong");
	Sandwich s20 = new Sandwich("Chicken terreyaky", 6.00, "6 inch");
	
	Sandwich s21 = new Sandwich("BBQ Pulled Pork", 7.50, "footlong");
	Sandwich s22 = new Sandwich("BBQ Pulled Pork", 6.00, "6 inch");
	
	Sandwich s23 = new Sandwich("Fist Of Meat", 9.00, "footlong");
	Sandwich s24 = new Sandwich("Fist Of Meat", 7.50, "6 inch");
	
	Sandwich s25 = new Sandwich("Meatball marrinara", 5.00, "footlong");
	Sandwich s26 = new Sandwich("Meatball marrinara", 4.50, "6 inch");
	
	sandwichDepot.add(s1);
	sandwichDepot.add(s2);
	sandwichDepot.add(s3);
	sandwichDepot.add(s4);
	sandwichDepot.add(s5);
	sandwichDepot.add(s6);
	sandwichDepot.add(s7);
	sandwichDepot.add(s8);
	sandwichDepot.add(s9);
	sandwichDepot.add(s10);		
	sandwichDepot.add(s11);
	sandwichDepot.add(s12);
	sandwichDepot.add(s13);
	sandwichDepot.add(s14);
	sandwichDepot.add(s15);
	sandwichDepot.add(s16);
	sandwichDepot.add(s17);
	sandwichDepot.add(s18);		
	sandwichDepot.add(s19);
	sandwichDepot.add(s20);
	sandwichDepot.add(s21);
	sandwichDepot.add(s22);
	sandwichDepot.add(s23);
	sandwichDepot.add(s24);
	sandwichDepot.add(s25);
	sandwichDepot.add(s26);
	
	sandwichDepot.remove(s25);
	
	sandwichDepot.update(s1, "new BMT", 7.77, "footlong");
	
	
	ArrayList<Sandwich> menu =(ArrayList<Sandwich>) sandwichDepot.getAll();
	for (Sandwich sandwich : menu) {
		System.out.println(sandwich);
	}
	System.out.println(menu.size());
	
	ArrayList<Sandwich> menuByPrice = (ArrayList<Sandwich>) sandwichDepot.getByPrice(5.00D);
	
	App.showList(menuByPrice, "by Price");
	System.out.println(sandwichDepot.getById(13));
	
	
	
	}

}
